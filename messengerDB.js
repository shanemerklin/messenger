const MongoClient = require('mongodb').MongoClient;
const bcrypt = require("bcryptjs");
const uri = "mongodb+srv://cloudLogin1:IhtfBFvAecGAmgHM@messenger.gpmdi.mongodb.net/messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}
const  checklogin = async (username,password)=>{
    //your implementation
    var users = getDb().collection("users");    
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username && user.admin == true && bcrypt.compareSync(password, user.password)){
        console.log("Debug>messengerdb.checklogin-> user found:\n" + JSON.stringify(user));
        return 2;
    }
    if(user!=null && user.username==username && user.allowed == true && bcrypt.compareSync(password, user.password)){
        console.log("Debug>messengerdb.checklogin-> user found:\n" + JSON.stringify(user));
        return 1;
    }
    return 0;
}


const addUser = async (username,password,firstName,lastName,userEmail,userPhone)=>{
    console.log("Debug>messengerdb.addUser:"+ username + "/" + password);
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username){
        console.log(`Debug>messengerdb.addUser: Username '${username}' exists!`);
    return "UserExist";
    }
    else{
        var hashedpassword = bcrypt.hashSync(password, 10);
        const newUser = {"username": username,"password" : hashedpassword, "admin": false, "allowed" : true, "firstName": firstName, "lastName": lastName, "email": userEmail, "PhoneNum": userPhone}
        try{
            const result = await users.insertOne(newUser);
            if(result!=null){
                console.log("Debug>messengerdb.addUser: a new user added: \n", result);
            return "Success";
            }
        }
        catch{
            console.log("Debug>messengerdb.addUser: error for adding '" + username +"':\n", err);
            return "Error";
        }
    }
}
const updateAccount = async(username,password,firstName,lastName,userEmail,userPhone)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    var hashedpassword = bcrypt.hashSync(password, 10);
    if(user!= null){
        var myquery = {username: username};
        if(password != ""){
            var newvalues = { $set: {"password" : hashedpassword, "firstName": firstName, "lastName": lastName, "email": userEmail, "PhoneNum": userPhone } };
        }
        else{
            var newvalues = { $set: {"firstName": firstName, "lastName": lastName, "email": userEmail, "PhoneNum": userPhone } };
        }
        users.updateOne(myquery, newvalues);
    }    
}
const loadAccountInfo = async (username) =>{
    var account_info = await getDb().collection("users").find({username:username}).toArray();
        return account_info
}

const blockUser = async (username) => {
    console.log("Debug>messengerdb.blockUser:"+ username);
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!= null){
        var myquery = {username: username};
        var newvalues = { $set: {allowed: false } };
        users.updateOne(myquery, newvalues);
    }
}

const silenceUser = async (username) => {
    console.log("Debug>messengerdb.silenceUser:"+ username);
    
    var pChat_history = await getDb().collection("private_chat");
    pChat_history.deleteMany({sender:username});
    pChat_history
    return true;
    
    
}


//To store friendship
const storeFriend = (user,friend)=>{
    console.log("DEBUG>> Storing friend to MongoDB");
   
    let chat = {user: user, friend: friend};
    try{
        const inserted = getDb().collection("friends").insertOne(chat);
        if(inserted!=null){
            console.log("Debug>messengerdb.storeFriend: a new friend was added: \n", JSON.stringify(chat));
        }
    }catch{
        console.log("Debug>messengerdb.storeFriend: error for adding '" + JSON.stringify(chat) +"'\n");
    }
}
const getFriendList = async (user)=>{
    var friend_list = await getDb().collection("friends").find(
        {user:user}).toArray();
    //print debug info e.g., using JSON.stringify(friend_list)        
    if (friend_list && friend_list.length > 0) return friend_list
}

//To store the data to a chat
const storePublicChat = (receiver,message,sender)=>{
    console.log("DEBUG>> Storing Public message to MongoDB");
    //TODO: validate the data
    let timestamp = Date.now();
    let chat = {receiver: receiver, sender: sender, message:message, timestamp:timestamp};
    try{
        const inserted = getDb().collection("public_chat").insertOne(chat);
        if(inserted!=null){
            console.log("Debug>messengerdb.storePublicChat: a new chat message added: \n", JSON.stringify(chat));
        }
    }catch{
        console.log("Debug>messengerdb.addUser: error for adding '" + JSON.stringify(chat) +"'\n");
    }
}
const loadChatHistory = async (receiver, limits=100)=> { //limits it to the last 100 messages
    var chat_history = await getDb().collection("public_chat").find(
        {receiver:receiver}).sort({timestamp:-1}).limit(limits).toArray();
        //print debug info e.g., using JSON.stringify(chat_history)
        if (chat_history && chat_history.length > 0) return chat_history
}
const storePrivateChat = (receiver,sender, partner, message,isSender)=>{
    console.log("DEBUG>> Storing Private message to MongoDB");
    //TODO: validate the data
    let timestamp = Date.now();
    let chat = {receiver: receiver, sender: sender, isSender: isSender, partner: partner, message:message, timestamp:timestamp};
    try{
        const inserted = getDb().collection("private_chat").insertOne(chat);
        if(inserted!=null){
            console.log("Debug>messengerdb.storePrivateChat: a new chat message added: \n", JSON.stringify(chat));
        }
    }catch{
        console.log("Debug>messengerdb.addUser: error for adding '" + JSON.stringify(chat) +"'\n");
    }
}
const loadPrivateChatHistory = async (receiver, sender, limits=100)=> { //limits it to the last 100 messages
    
    var privateChat_history = await getDb().collection("private_chat").find({
        $or:[ {sender:receiver, receiver:sender}, {sender:sender, receiver:sender, partner: receiver} ]}).sort({timestamp:-1}).limit(limits).toArray();
        //print debug info e.g., using JSON.stringify(chat_history)
        if (privateChat_history && privateChat_history.length > 0){
            return privateChat_history
        } 
          
}
const storeGroupChat = (sender, groupName, message, isSender)=>{
    console.log("DEBUG>> Storing group message to MongoDB");
    //TODO: validate the data
    let timestamp = Date.now();
    let chat = {sender: sender, groupName: groupName, isSender: isSender, message:message, timestamp:timestamp};
    try{
        const inserted = getDb().collection("group_chat").insertOne(chat);
        if(inserted!=null){
            console.log("Debug>messengerdb.storeGroupChat: a new chat message added: \n", JSON.stringify(chat));
        }
    }catch{
        console.log("Debug>messengerdb.addUser: error for adding '" + JSON.stringify(chat) +"'\n");
    }
}
const loadGroupChatHistory = async (groupName, limits=100)=> { //limits it to the last 100 messages
    //this below is far from finished but im trying to hard code for now
    /*var groupUsersList = JSON.parse(jsonGroupUsers)
   var groupChat_history;
    for(var i = 0; i < groupUsersList.users.length; i++) {
        console.log("test1");
        var users = groupUsersList.users[i];
        console.log("users: " + users + ", i is: " + i);
        for(const key in users) {
           
            var user = users[key];
            console.log(user + " is " + key);*/
               
                  var groupChat_history = await getDb().collection("group_chat").find({
                    $or:[ {groupName: groupName}]}).sort({timestamp:-1}).limit(limits).toArray();
                    if (groupChat_history && groupChat_history.length > 0){
                        console.log(groupChat_history);
                        return groupChat_history
                    } 
  
          
}

module.exports = {checklogin, addUser, blockUser, silenceUser, storePublicChat, loadChatHistory, storePrivateChat, loadPrivateChatHistory, storeGroupChat, loadGroupChatHistory, updateAccount, loadAccountInfo, storeFriend, getFriendList};