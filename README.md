# README.md - CPS490 Report - Team 9

Source: <https://bitbucket.org/capstones-cs-udayton/cps490/src/master/README.md>

University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Fall 2021

Instructor: Dr. Phu Phung


## Capstone I Project 


# The Messenger Application


# Team members

1.  Matthew Mark        <markm1@udayton.edu>
2.  Shane Merklin       <merklins1@udayton.edu>
3.  Emma Haynes         <haynese5@udayton.edu>
4.  Alejandro Ruiz      <ruiza9@udayton.edu>


# Project Management Information

Management board (private access): <https://trello.com/b/M9oMr4iC/cps490f21-team9>

Source code repository (private access): <https://bitbucket.org/cps490f21-team9/cps490-project-team9/src/master/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|09/09/2021|  0.1          | Init draft   |
|09/10/2021|  0.2          | SPR 0 COMPL  |
|09/24/2021|  0.3          | SPR 0 REV    |
|10/04/2021|  0.4          | SPR 1 COMPL  |
|10/28/2021|  0.5          | SPR 2 COMPL  |
|12/02/2021|  0.6          | SPR 3 COMPL  |


# Overview

The goal of this project is to provide our client with a messenger application. This application will be used on by the users on a client device that is connected to a web server.
For this teams project our messenger application will be geared towards group messenger application. A user will have the ability to create a group and have multiple subgroups betweem the people in their organization. 

![](https://trello.com/1/cards/617b6123c92b5e7bb2559c5e/attachments/617b6123c92b5e7bb2559c69/download/image.png)



# System Analysis
The program will be set-up to easily be able to create accounts with minimal information. This is done in part with the fact that the messengar application that we create is for a public and private internet chat system.
The program will alow users to connect with anyone in a public chat as well as in groupchats and private messages. Users will be able to make friendships and connect through messages in the program with real time status. The program will allow for various groupchat and admin features to control chats.
Users will be fill out basic information for to identify who they are to other people in the chats.
All of this will be stored in a data base and be secured safely.
 
## User Requirements
 
* Users must be able to identify between admin and general users
* Users must be able to create a groupchat and add multiple users to the chat from a User List
* Users must be able to go inbetween different subchats in the groupchat
* Admin must be able to assign the Members of certian groupchats
* Admin can terminate an account at any moment except the admin account 

* Real time typing updates will apear when User is typing
* User will have the option to choose between Public and Private Chat before messaging 
* User will be able to choose from a list of users to talk to
* Users must be able to register for an account 
* Users must be able to sign in with a Username and a Password
* Private Chat users that are logged in will be the only ones to recieve message 
* Users will be able to Logout 
* Users will have the ability to input first and last name 
* Users will have the ability to input contact information (email, phone number)
* Users will be able to see and change the password they have for their login

## Use Story
Three people want to use our platform to message each other when they are not together. 
They can securely access our platform to send and recieve messages to friends and create groupchats. inputing data to better identify with each other as well as offering public chats to meet new people on a server.
## Use cases

![](https://trello.com/1/cards/617b6183a52a39830f6bffc3/attachments/617b6183a52a39830f6bffce/download/image.png)

* Create Admin Account
    - Actor: Unregistered User
    - Description: Admin Account allows user to create the group chat also sub chats in the main group
* Create General Account
    - Actor: Unregistered User
    - Description: General users are invited to join groups and subgroups
* Send Messages
    - Actor: Admin User or Normal User
    - Description: All users can send messages in their appropriate chat
* Receive Messages
    - Actor: Admin User or Normal User
    - Description: Users are able to receive messages from their appropriate chats
* View Messages
    - Actor: Admin User or Normal User
    - Description: Users are able to see messages from the group and the subgroups they have access too
* Create Groups
    - Actor: Admin User
    - Description: Only an Admin Account is allowed to create groupchats along with subgroups
* Remove User from Group
    - Actor: Admin User
    - Description: The Admin that is responsible for creating the groupchat has authority to remove users from the group
* Add User to Group
    - Actor: Admin User
    - Description: Admin has the ability to invite users to their group 
* Find User
    - Actor: Admin User
    - Description: All users are able to search for other users based on their username
* Add User as Friend
    - Actor: Admin User or Normal User
    - Description: All users are able to add users as friends.
* Toggle between screen backgrounds 
    - Actor: Admin or Normal User
    - Description: Users can choose between a light blue background or black background 
* Admin Remove user
    - Actor: Groupchat admin user
    - Description: Groupchat admin will have the ability to select and remove users from a private chat
* User React to message
    -Actor: Any User
    -Description: Users will have the ability to react to a message with an emoji


## Detailed Use Case Description Example
**Use case:** U1 Send Messages

**Scenario:** Actor wants to send message into group chat

**Triggering event:** Actor selects send button

**Brief description:** When user enters a group chat, they can type in type-bar. Then after the triggering event is initiated the message will be sent to all members of the group.

**Actor:** Admin User or Normal User

**Stakeholders:** Send Message to Receive

**Preconditions:** Message must be typed out

**Post Conditions:** The system will initiate the process of receiving messages from the other user’s side (other users in the group chat).

**Actor:**

    1	Actor uses type-bar
    2	Actor selects the “send” button

**System:**

    1.1 System displays the message being typed
    2.1 System will display message in group chat
    2.2 System sends message to Receiver

**Exception Conditions:**

    1a. No message to be sent

**Use Case:** Create a groupchat from a User List
**Scenario:** User wants to create a groupchat from the active users on the Public messenger
**Triggering Event:** Actor chooses multiple actors from the user list to create a groupchat with the 'Create Groupchat' button
**Brief Description:** An Actor that wants to create a group of public users can choose what actors are originally added and more actors can join the public group
**Actor:**
1   Actor selects the “Create Group” button
2   Actor selects multiple actors from the User List
**System:**
1.1 System places actors are placed in a group 
2.1 System creates 'welcome' message for group actors  
2.2 Group-chat IDnumber will be shown to invite more members
**Exception Conditions:**
1a. Users cannot join without invitation from public group
1.a Users can be public or private(logged in) but the chat allows public users of messenger

**Use Case:** Login with Username and Password

**Scenario:** Actor is asked to input a Username and Password to login to a private chat

**Triggering Event:** Actor presses login button and is prompted the username box and password box

**Brief Description:** An Actor will prompt up the sight and they will have to sign in to be able to message privately by providing the correct Username and Password

**Actor:** 

    1	Actor selects the “Login” button
    2	Actor uses valid Username
    3   Actor uses a valid Password
    2	Actor selects the “Login” button

**System:**

    1.1 System displays the Username and Password text box's
    2.1 System validates the username  
    2.2 System validates the password

**Exception Conditions:**

    1a. User cannot be found (+ register User)
    1b. User password incorrect
    1c. User username incorrect
**Use Case:** Create a groupchat from a User List 

**Scenario:** User wants to create a groupchat from the active users on the Public messenger 

**Triggering Event:** Actor chooses multiple actors from the user list to create a groupchat with the 'Create Groupchat' button 

**Brief Description:** An Actor that wants to create a group of public users can choose what actors are originally added and more actors can join the public group

**Actor:** 

    1	Actor selects the “Create Group” button
    2	Actor selects multiple actors from the User List 

**System:**

    1.1 System places ctors are placed in a group 
    2.1 System creates 'welcome' message for group actors  
    2.2 Groupchat IDnumber will be shown to invite more members

**Exception Conditions:**

    1a. Users cannot join without invitation from public group
    1.a Users can be public or private(logged in) but the chat allows public users of messenger
    




**Use Case:** Kick Account 
**Scenario:** Admin wants to kick a user from a chat
**Triggering Event:** Admin confirms the user that they want to kick out 
**Brief Description:** An Admin can kick someone from the group using the kick user drop down on the left and logs user out
**Actor:**
1.1 Admin logs in to create group
1.2 Actor logs into group
1.3 Admin kicks user out of group
**System:** 
1.2 System provides list to kick users from chat
1.3 System removes basic user
1.4 System is logs out user

**Exception Conditions:**
1.a User tries to kick user and is not a Admin

**Use Case:** Light and Dark mode toggle 
**Scenario:** User wants to turn from light screen mode to a darker screen mode
**Triggering Event:** User presses button that calls for the screen to switch colors to a darker or lighter screen
**Brief Description:** An actor is in a dark setting and wants to limit the color contrast by prompting the dark mode
**Actor:**
1.1 Actor wants to make screen darker
1.2 Actor wants a brighter screen color 
**System:**
1.2 System calls for the colors that the screen shows
1.3 Dark colors are blocked when the toggle is set to light 
1.4 Light colors are blocked when the toggle is set to dark 
**Exception Conditions:**
1.a User does not toggle switch and light mode is default 

**Use Case:** Register account
**Scenario:** User does not have an account, chooses to register for account
**Triggering Event:** User presses on register and inputs information 
**Brief Description:** User does not have an account, presses toggle and creates account to use messenger
**Actor:**
1.1 Actor wants to create account
1.2 Inputs username
1.3 Inputs password
**System:**
1.2 System asks for username and password
1.3 System creates a user in the database
1.4 System goes to login to be able to access messenger

**Exception Conditions:** 
1.a User provides invalid input to password
1.b User provides a username that is already in use

# System Design


## Use-Case Realization

![](https://trello.com/1/cards/617b61cb4aa8293cfe465a72/attachments/617b61cb4aa8293cfe465a7d/download/image.png)

## Database 

## User Interface

![](https://trello.com/1/cards/617b6169826f2f8306ff30d5/attachments/617b6169826f2f8306ff30e0/download/image.png)

# Implementation

![](https://trello.com/1/cards/617b61ba87fbfb644e8fc165/attachments/617b61ba87fbfb644e8fc170/download/image.png)
![](https://trello.com/1/cards/617b61a43ea0587d9a4429fb/attachments/617b61a43ea0587d9a442a06/download/image.png)

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 


Code Snippet for the Groupchats to be able to form among mutiple people
![](https://trello.com/1/cards/61a809356632ad1986589281/attachments/61a8093f9b1fff469be0afe4/download/Screen_Shot_2021-12-01_at_6.42.55_PM.png)
Code Snippet for an Admin to be able to kick a user from a groupchat
![](https://trello.com/1/cards/61a80930ede16b47b7368d40/attachments/61a80a33521a065350f90972/download/Screen_Shot_2021-12-01_at_6.43.06_PM.png)
Code Snippet for private messenger chats for 1 on 1 use
![](https://trello.com/1/cards/61a80f44e17652861c883e74/attachments/61a810262f6521486d883741/download/1on1.png)
Code Snippet for Chat History
![](https://trello.com/1/cards/61a8108eaeeba82350e01930/attachments/61a810ed0597ad2dbd82a31c/download/PrivatePublicChatHistory.png)
Code snippet for typing status for both private and public messages
![](https://trello.com/1/cards/61a8108eaeeba82350e01930/attachments/61a810ed0597ad2dbd82a31c/download/PrivatePublicChatHistory.png)


## Deployment

Describe how to deploy your system in a specific platform.
Our system is deployed using heroku. Using "git" the files ChatServer.js and index.html are sent to the heroku platform. The heroku platform currently runs our application to public using the link below:
https://messenger-team9.herokuapp.com/

# Software Process Management

## Scrum process

### Sprint 0

Duration: 08/26/2021-09/10/2021

#### Completed Tasks: 

    User Case Diagram : Shane
    Trello Board/Bitbucket: Shane, Alejandro, Matt, Emma
    Trello Use Case Cards: Shane, Alejandro, Matt, Emma
    Trello Board Slide: Alejandro
    Daily Schedule: Matt
    Project Timeline slide: Emma


#### Contributions: 

1.  Matthew Mark, 2 hours, contributed in Trello, Bitbucket, Use Case Diagram
2.  Shane Merklin, 2 hours, contributed in Trello, Bitbucket, Use Case Diagram, User Story, Detailed Use Case Description
3.  Emma Haynes, 2 hours, contributed in Trello, Bitbucket, Use Case Cards
4.  Alejandro Ruiz, 2 hours, contributed Trello, Bitbucket, Use Case Cards



### Sprint 1

Duration: 09/10/2021-10/04/2021

#### Completed Tasks: 

   Creation of Login and  Public Chat UI's
   Seperate UI's for Public and Private Chat
   Use of CSS and an external library
   Real-Time Typing Status to Public Chat
   Maintaining a user list
   Login with a user name, and ability to send / receive messages
   Application is deployed on heroku


#### Contributions: 

1.  Matthew Mark - Development of Public Chat Functionality and Real-Time Typing, Private Chat, ReadME File Updates
2.  Shane Merklin - Development of Public Chat UI, use of External Library
3.  Emma Haynes - Development of Private Chat UI, Private Chat Message Board
4.  Alejandro Ruiz - Presentation Preparation

### Sprint 2

Duration: 10/06/2021-10/28/2021

#### Completed Tasks: 

   Creation of Login and  Public Chat UI's
   Seperate UI's for Public and Private Chat and Group Chat
   Use of CSS and an external library
   Real-Time Typing Status to Public Chat Private and Group Chat
   Maintaining a user list
   Login with a user name that gets stored into the Mongo Data Base
   Application is deployed on heroku

### Sprint 3

Duration: 10/09/2021-12/02/2021

#### Completed Tasks: 

   Added Docker file
   Favorites
   Admin privileges
   Implemented Database
   


#### Contributions: 

1.  Matthew Mark - Development of adding data to Mongo Data Base, ReadME File Updates, implementation of usecases
2.  Shane Merklin - Development of Private UI, Group Chat UI, use of External Library, implementation of usecases
3.  Emma Haynes - Development of Group  Chat Functionality, Group Chat UI, Group Chat Message Board, ReadME File Updates
4.  Alejandro Ruiz - Backend Development of use cases, updating ReadDME, Trello updates, diagrams 

## Sprint Retrospective:

### Sprint 0 (Ended on 9/10/2021)

| Good          |   Could have been better              |  How to improve?                  |
|---------------|:-------------------------------------:|----------------------------------:|
|               |Communication on what needs to be done |Organize tasks for each team member|


### Sprint 1 (Ended on 10/4/2021)
| Good          |   Could have been better              |  How to improve?                  |
|------------------------------------|:-------------------------------------:|----------------------------------:|
|Found out our strenghts and weakness|Communication on what needs to be done |Organize tasks for each team member| 
|Delegation the workload to suit the |                                       |                                   |
|team members skills                 |                                       |                                   |


### Sprint 2 (Ended on 10/28/2021)

| Good                             |   Could have been better              |  How to improve?                  |
|----------------------------------|:-------------------------------------:|----------------------------------:|
|Had more meetings to disscus the  |Communication on what needs to be done |Organize tasks for each team member|
|functionality of the site         |Divide the tasks into more managable   |Have each team member be at the    |
|                                    |parts                                  |meetings.                          |


### Sprint 3 (Ended on 12/02/2021)

| Good                             |   Could have been better              |  How to improve?                  |
|----------------------------------|:-------------------------------------:|----------------------------------:|
|We all knew what needed to be done|Communication on what needs to be done |Give out dead line for task tasks  |
|Had more confidence in our ability|Divide the tasks into more managable   |Make sure that everyone knows what |
|to complete the project           |parts                                  |they are doing                     |



# User guide/Demo
Below are screenshots that demostrate how to use our app along with all the different unqinue use cases that we delevoped ourselves.


Login screen showing the new UI
![](https://trello.com/1/cards/617b5eddf845e0248dfe2551/attachments/617b5eddf845e0248dfe255c/download/image.png)
The Chat UI after the login is passed
![](https://trello.com/1/cards/617b5f4fb0cc4e48c3379ebd/attachments/617b5f4fb0cc4e48c3379ec8/download/image.png)
The Light screen toggle(default setting when opening messenger)
![](https://trello.com/1/cards/617b5ebbab37302d740c6932/attachments/617b5ebbab37302d740c693d/download/image.png)
The dark screen toggle 
![](https://trello.com/1/cards/617b5e721e9b980272848fee/attachments/617b5e721e9b980272848ff9/download/image.png)
When a user does not supply the correct login information, Invalid login response
![](https://trello.com/1/cards/617b5e81c8039e401184d38b/attachments/617b5e81c8039e401184d3a8/download/image.png)
When a user wants to register for an account and they input valid information
![](https://trello.com/1/cards/617b5f2f9f3e795ba2c191bc/attachments/617b5f2f9f3e795ba2c191c7/download/image.png)
Two Users have a chat in the private message box and show the private message UI for messenger
![](https://trello.com/1/cards/617b5ef7aae58b3816b5c9dc/attachments/617b5ef7aae58b3816b5c9e7/download/image.png)
![](https://trello.com/1/cards/617b5f0e06b803829fb0bb9c/attachments/617b5f0e06b803829fb0bbc9/download/image.png)
Edit User Info
![](https://trello.com/1/cards/61a805351945253baf51202f/attachments/61a8054b572dfb285486bf9d/download/EditUser.png)


# Trello Diagram
![](https://trello.com/1/cards/617b61476de4c10ab275e3d1/attachments/617b61476de4c10ab275e3dc/download/image.png)

# Updated Trello Diagram after sprint 2
![](https://trello.com/1/cards/617b5f6d0e1f713a98ee67ca/attachments/617b5f6d0e1f713a98ee67d5/download/image.png)
![](https://trello.com/1/cards/617b5f92c8314f88d89dff82/attachments/617b5f92c8314f88d89dff90/download/image.png)

# Updated Trello Diagram after sprint 3
![](https://trello.com/1/cards/61a82a40de7a1d6d96501f7a/attachments/61a82a5cae3ba31793b64dd8/download/Screen_Shot_2021-12-01_at_9.06.32_PM.png)

