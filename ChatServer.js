// --------------------------  Set-up --------------------------

var xssfilter = require("xss");
var http = require('http')
var app = require('express')()
var server = http.createServer(app)
const port = process.env.PORT || 8080
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`)
app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname+'/index.html')
})
var io = require('socket.io');
var socketio = io.listen(server);


console.log("Socket.IO is listening at port: " + port);

// <<<<<<<<<<<<<<<<<<<<<<<<<< SOCKET.ON CONNECTION >>>>>>>>>>>>>>>>>>>>>>>>>>
socketio.on("connection", function(socketclient) {
    console.log("A new Socket.IO client is connected. ID = " + socketclient.id);
    socketclient.username;
    // --------------------------  Login --------------------------
    socketclient.on("login", async(username, password) => {
        console.log("Debug>Got username="+username);
        var checklogin = await DataLayer.checklogin(username,password);
        console.log("CheckLogin Value:" + checklogin);
        if(checklogin == 2){
            console.log("Got 2");
            socketclient.authenticated = true;
            socketclient.admin = true;
            socketclient.emit("authenticated");
            socketclient.emit("authenticatedAdmin");            
            socketclient.username = username;
            socketclient.password = password;
            console.log("Username entered= " + username, "Password enterted= " + password);
            var welcomemessage = username + " has joined this message board!";
            console.log(welcomemessage);
            //socketio.sockets.emit("serverReply", welcomemessage);
            
            var chat_history = await messengerdb.loadChatHistory(username)
                if (chat_history && chat_history.length > 0) {
                    chat_history = chat_history.reverse()
                    //reverse the order as we get the latest first
                    socketclient.emit("chat_history", (chat_history))
            }
            SendToAuthenticatedClient(socketclient,"serverReply", welcomemessage);            
            updateUserList();
        }
        else if(checklogin == 1){
            console.log("Got 1");
            socketclient.authenticated = true;
            socketclient.admin = false;
            socketclient.emit("authenticated");
            socketclient.username = username;
            socketclient.password = password;
            console.log("Username entered= " + username, "Password enterted= " + password);
            var welcomemessage = username + " has joined this message board!";
            console.log(welcomemessage);
            //socketio.sockets.emit("serverReply", welcomemessage);
            //SendToAuthenticatedClient(socketclient,"serverReply", welcomemessage);
           // updateUserList();
            var chat_history = await messengerdb.loadChatHistory(username)
                if (chat_history && chat_history.length > 0) {
                    chat_history = chat_history.reverse()
                    //reverse the order as we get the latest first                   
                    socketclient.emit("chat_history", (chat_history))
                }
            SendToAuthenticatedClient(socketclient,"serverReply", welcomemessage);
            updateUserList();
            var friend_list = await messengerdb.getFriendList(username)
                    socketclient.emit("display_Friends", (friend_list))
        }
        else if(checklogin == 0){
            console.log("Got 0");
            socketclient.emit("invalid_login");
            updateUserList();
        }
        updateUserList();
    });

    // --------------------------  Registration --------------------------
    socketclient.on("register", async (username,password,firstName,lastName,userEmail,userPhone)=> {
        //Call the Data Layer to register
        const registation_result = await DataLayer.addUser(username,password,firstName,lastName,userEmail,userPhone);
        socketclient.emit("registration", (registation_result))
    });    


    // --------------------------  Logout --------------------------
    socketclient.on("logout", () => {
        username = socketclient.username;
        var logoutmessage = username + " has left this messenger board...";
        console.log(logoutmessage);
        if(username != undefined){
            //socketio.sockets.emit("serverReply", xssfilter(logoutmessage));
            SendChatToAuthenticatedClient(socketclient,"serverReply", logoutmessage);  
        }
        socketclient.username = null;
        console.log("Debug ---->", socketclient.username);
        updateUserList();                
    });

    // --------------------------  Socket Disconnection --------------------------
    socketclient.on("disconnect", (reason) => {
        console.log("A Socket.IO client has disconnected. ID = " + socketclient.id);
        username = socketclient.username;
        var message = username + " has left.";
            console.log(message);
            console.log(reason);
            //var sockets = socketio.sockets.sockets;
            if(username != undefined){
                SendChatToAuthenticatedClient(socketclient,"serverReply",message);                  
                //socketio.sockets.emit("serverReply", xssfilter(message));
            }
            updateUserList(); 
    });
    
    // --------------------------  Public Chat --------------------------
    socketclient.on("chat", (message) => {
        if(!socketclient.authenticated){
            console.log("Unauthenticated client sent a chat. Supress!");
            return;
        } 
        
        var chatmessage = socketclient.username + ": " + message;
        console.log(chatmessage);
        //socketio.sockets.emit("serverReply", chatmessage);
        SendChatToAuthenticatedClient(socketclient,"serverReply",chatmessage);      
    });
    // --------------------------  Real Time PUBLIC Typing Status --------------------------        
    socketclient.on("<TYPE>", function(){
        //if(isNullOrUndefined(socketclient.username)){
         //   console.log("Undefined - matt");
         //   return;
       // }
        var msg = socketclient.username;
        socketio.sockets.emit("<TYPING>", xssfilter(msg));
        //console.log("[<TYPING>," + msg + "] is sent to all connected clients");
        socketio.sockets.emit("user_typing", xssfilter(msg));
    });

    // --------------------------  Real Time PRIVATE Typing Status --------------------------        
    socketclient.on("<PRIVATE-TYPE>", (receiver)=>{
        sender = socketclient.username;
        console.log("Sender: " + sender + " / Receiver: " + receiver);
        var sockets = socketio.sockets.sockets;
        for(var socketId in sockets){
            var sclient = sockets[socketId];
            if(sclient.username==receiver){
                console.log("Send Type to Receiver");                
                var msg = sender;
                sclient.emit("user_typing", xssfilter(msg));
            }
            if(sclient.username==sender){
                console.log("Send Type to Sender");
                var msg = sender;
                sclient.emit("user_typing", xssfilter(msg));
            }
        }
    
        
    //     //if(isNullOrUndefined(socketclient.username)){
    //      //   console.log("Undefined - matt");
    //      //   return;
    //    // }
    //    SendToAuthenticatedClient(socketclient, , )
    //     var msg = socketclient.username;
    //     socketio.sockets.emit("<PRIVATE-TYPING>", msg);
    //     //console.log("[<TYPING>," + msg + "] is sent to all connected clients");
    //     socketio.sockets.emit("user_typing", msg);
    });
    // --------------------------  1 on 1 Private Chat --------------------------
    socketclient.on("privateChat", (privateMessage, privateUser) => {
        var consolemessage = socketclient.username + " says: " + privateMessage + " to: " + privateUser;
        var chatmessage = socketclient.username + " says: " + privateMessage;
        console.log(consolemessage);
        SendToPrivateClient(chatmessage,privateUser,socketclient.username);
        
    });

    // --------------------------  GroupChat --------------------------
    //GroupChat *** IN PROGRESS***
    //needs the list of users to loop through and send the messages 
    //to every user in the group
    socketclient.on("groupMessage", (groupMessage, jsonGroupUsers, groupName) => {
        var chatmessage = socketclient.username + " says: " + groupMessage;
        var consolemessage = socketclient.username + " says: " + groupMessage + " to: " + JSON.stringify(jsonGroupUsers);
        console.log(consolemessage);
        var groupUsersList = JSON.parse(jsonGroupUsers)
        console.log("groupUsersArray.length="+groupUsersList.users.length);
        SendToGroupClient(chatmessage,groupUsersList,socketclient.username, groupName);

    });
    
    // --------------------------  Admin Functions (Kick) --------------------------
    socketclient.on("kickUser", async (username) => {
        if(socketclient.admin = true){
            var sockets = socketio.sockets.sockets;
            for(var socketId in sockets){
                var sclient = sockets[socketId];
                if(sclient.username==username){
                    console.log("Kick User: ", username);                
                    const result = await DataLayer.blockUser(username);
                    console.log("Result => ", result);
                    sclient.emit("kickMe");
                }
            }
        }
        
        
    });

    // --------------------------  Admin Functions (Silence) --------------------------
    socketclient.on("silenceUser", async (username) => {
        if(socketclient.admin = true){
            var sockets = socketio.sockets.sockets;
            for(var socketId in sockets){
                var sclient = sockets[socketId];
                if(sclient.username==username){
                    console.log("Silence User: ", username);                
                    const result = await DataLayer.silenceUser(username);
                    console.log("Result => ", result);
                    //sclient.emit("silenceMe");
                }
            }
        }
        
        
    });

    //---------------------------Adding a new Friend--------------------------------------
    socketclient.on("addNewFriend", async (username, friend) => {
        await messengerdb.storeFriend(username, friend);
        var friend_list = await messengerdb.getFriendList(username)
            socketclient.emit("display_Friends", (friend_list))

    })
    //---------------------------Getting Account Information -----------------------------

    socketclient.on("getAccountInfo", async (username) => {
        var account_info = await messengerdb.loadAccountInfo(username)
        socketclient.emit("account_history", (account_info));
    })
    //--------------------------Updating Account Information------------------------------
    socketclient.on("updateAccount", async (username, password, firstname, lastname, newemail, newphone) => {
       await messengerdb.updateAccount(username, password, firstname, lastname, newemail, newphone);
    })
    // --------------------------  Getting Private Chat History --------------------------
    socketclient.on("getPrivateHistory", async (receiver, username) => {
        
        var privateChat_history = await messengerdb.loadPrivateChatHistory(receiver, username)
        if (privateChat_history && privateChat_history.length > 0) {
            privateChat_history = privateChat_history.reverse()
            //reverse the order as we get the latest first

            socketclient.emit("privateChat_history", (privateChat_history));
        }
    })   
     // --------------------------  Getting Group Chat History --------------------------
     socketclient.on("getGroupHistory", async (groupName) => {
        
        var groupChat_history = await messengerdb.loadGroupChatHistory(groupName)
        if (groupChat_history && groupChat_history.length > 0) {
            groupChat_history = groupChat_history.reverse()
            //reverse the order as we get the latest first

            socketclient.emit("groupChat_history", (groupChat_history));
        }
    })   
  

// <<<<<<<<<<<<<<<<<<<<<<<<<< END OF SOCKET.ON CONNECTION >>>>>>>>>>>>>>>>>>>>>>>>>>
});

// --------------------------  1 on 1 Private Chat --------------------------
function SendToPrivateClient(message,receiver,sender){
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
        var socketclient = sockets[socketId];
        if(socketclient.username==receiver){
            socketclient.emit("privateServerReply", xssfilter(message));
            var logmsg= "Debug:>sent to " + socketclient.username + " with ID=" + socketId;
            console.log(logmsg);
            messengerdb.storePrivateChat(socketclient.username, sender, sender, message, false); //to store the chat message
        }
        if(socketclient.username==sender){
            //socketclient.emit("privateServerReply",message); (dont need this because we are already sending it in sendPrivateMessage)
            var logmsg= "Debug:>sent to " + socketclient.username + " with ID=" + socketId;
            console.log(logmsg);
            messengerdb.storePrivateChat(socketclient.username, socketclient.username, receiver, message, true); //to store the chat message
        }
    }
}
 // --------------------------  Group Chat -------------------------- 
//Start function of groupchat client *** IN PROGRESS***
// would need to use the list of string of usernames? 
function SendToGroupClient(message,groupUsersList,sender, groupName){
    console.log("test");
    
    messengerdb.storeGroupChat(sender, groupName, message, true); //to store the chat message
    
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
        var socketclient = sockets[socketId];
        

        //would need a for loop to go through all the users who were added to the groupchat
        for(var i = 0; i < groupUsersList.users.length; i++) {
            console.log("test1");            
            var users = groupUsersList.users[i];
            for(const key in users) {
                console.log("test2");
                var user = users[key];
                
                if(socketclient.username==user){
                    socketclient.emit("groupServerReply", xssfilter(message));
                    //var logmsg= "Debug1:>sent to " + user + " with ID=" + socketId;
                    
                   // messengerdb.storeGroupChat(socketclient.username, groupName, message, false); //to store the chat message
                }
                if(socketclient.username==sender){
                    //socketclient.emit("groupSeverReply",message); (dont need this because we are already sending it in sendGroupChat)
                    //var logmsg= "Debug2:>sent to " + user + " with ID=" + socketId;
                    //messengerdb.storeGroupChat(socketclient.username, groupName, message, true); //to store the chat message
                }
                
            }
        }
    }
}

// --------------------------  Data Layer & Authentication --------------------------

var messengerdb=require("./messengerDB");
var DataLayer = {
    info: 'Data Layer Implementation for Messenger',
    async checklogin(username, password){
        var checklogin_result = await messengerdb.checklogin(username,password);
        console.log("Debug>DataLayer.checklogin->result="+ checklogin_result);
        return checklogin_result;
    }
    ,
    async addUser(username,password,firstName,lastName,userEmail,userPhone){
        const result = await
        messengerdb.addUser(username,password,firstName,lastName,userEmail,userPhone);
        return result;
    }
    ,
    async blockUser(username){
        const result = await
        messengerdb.blockUser(username);
        return result;
    }
    ,
    async silenceUser(username){
        const result = await
        messengerdb.silenceUser(username);
        console.log("Results of Silence: " + result);
        return result;
    }                                 
}

function SendToAuthenticatedClient(sendersocket,type,data){
    console.log("Send to Called ~ SS[" + sendersocket + "], TYPE[" + type + "], Data[" + data + "]");
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
            var sclient = sockets[socketId];
            //this if statement is to not send the message again to the sender (this line is the only thing I added [shane])
            
            if(sclient.authenticated){
                sclient.emit(type, xssfilter(data));
                var logmsg = "Debug:>sent to " + sclient.username + " with ID=" + socketId;
                console.log(logmsg);
                
            }
        
    }
}


function SendChatToAuthenticatedClient(sendersocket,type,data){
    console.log("Send to Called ~ SS[" + sendersocket + "], TYPE[" + type + "], Data[" + data + "]");
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
            var sclient = sockets[socketId];
            //this if statement is to not send the message again to the sender (this line is the only thing I added [shane])
            
            if(sclient.authenticated && sclient.username != sendersocket.username){
                sclient.emit(type, xssfilter(data));
                var logmsg = "Debug:>sent to " + sclient.username + " with ID=" + socketId;
                console.log(logmsg);
                messengerdb.storePublicChat(sclient.username, data, false); //to store the chat message
                
            }
            if(sclient.authenticated && sclient.username == sendersocket.username){ //to store the senders message seperatly
                messengerdb.storePublicChat(sclient.username, data, true); //to store the chat message (true means that this message is from the sender)
                
            }
        
    }
}

// --------------------------  Maintain User List --------------------------

function updateUserList(){
    var list = "";
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
        var sclient = sockets[socketId];
        if(sclient.username != null){
            list = list + "<br>" + sclient.username;
        }
    }
    console.log(list);
    SendToAuthenticatedClient(undefined, "updateList", list);
}