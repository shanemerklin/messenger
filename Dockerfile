# syntax=docker/dockerfile:1
FROM node:12
#Create app directory
WORKDIR /usr/scr/app

#Install app dependencies 
COPY package*.json ./
RUN npm install

#Bundle the app source
COPY . .
#Just for CPS490 Lab5
RUN echo "Creating a Docker image by Team 9: Chatter"

# the command to execute the app
CMD [ "npm", "start" ]